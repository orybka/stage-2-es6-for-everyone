import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createElement({
    tagName: 'div',
    className: 'winner-btn-container'
  });
  const title = `${fighter.name} is a winner!`;

  const winnerBtn = createElement({
    tagName: 'button',
    className: 'winner-btn'
  });
  winnerBtn.innerText = 'Play again';
  bodyElement.append(winnerBtn);

  const newGame = () => {
    window.location.reload();
  };
  winnerBtn.addEventListener('click', newGame);

  showModal({title, bodyElement, newGame});
}
