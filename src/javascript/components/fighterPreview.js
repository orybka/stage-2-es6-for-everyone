import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fighterInfo = createElement({
      tagName: 'div',
      className: 'fighter-container'
    });
    const fighterImageContainer = createElement({
      tagName: 'div',
      className: 'fighter-image-container'
    });

    const name = createElement({
      tagName: 'h2',
      className: 'fighter-name',
    });
    name.innerText = fighter.name;

    const health = createElement({
      tagName: 'p',
      className: 'fighter-details',
    });
    health.innerText = `Health : ${fighter.health}`;

    const attack = createElement({
      tagName: 'p',
      className: 'fighter-details',
    });
    attack.innerText = `Attack : ${fighter.attack}`;

    const defense = createElement({
      tagName: 'p',
      className: 'fighter-details',
    });
    defense.innerText = `Defense : ${fighter.defense}`;

    const image = createElement({
      tagName: 'img',
      className: 'fighter-image'
    });
    image.src = fighter.source;
    image.alt = fighter.name;

    fighterImageContainer.append(image);
    fighterInfo.append(name, health, attack, defense);
    fighterElement.append(fighterImageContainer, fighterInfo);
  }


  // source: "https://media1.giphy.co
  

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
