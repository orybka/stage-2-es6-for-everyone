import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const game = {
      firstPlayer: {
        ...firstFighter,
        currentHealth: firstFighter.health,
        blocked: false,
        keyOrderNum: -1,
        hitTime: 0,
        bar: document.querySelector('#left-fighter-indicator'),
      },
      secondPlayer: {
        ...secondFighter,
        currentHealth: secondFighter.health,
        blocked: false,
        keyOrderNum: -1,
        hitTime: 0,
        bar: document.querySelector('#right-fighter-indicator'),
      }
    };
    document.addEventListener('keydown', (event) => {
      const keyPressed = event.code;

      switch (keyPressed) {
        case controls.PlayerOneBlock:
          game.firstPlayer.blocked = true;
          break;
        case controls.PlayerTwoBlock:
          game.secondPlayer.blocked = true;
          break;
        case controls.PlayerOneAttack:
          hitPlayer(game.firstPlayer, game.secondPlayer);
          break;
        case controls.PlayerTwoAttack:
          hitPlayer(game.secondPlayer, game.firstPlayer);
          break;
      }

      const checkPlayerKey = checkKeys(keyPressed);

      if (checkPlayerKey === 'first') {
        criticalHitPlayer(game.firstPlayer, game.secondPlayer);
      } else if (checkPlayerKey === 'second') {
        criticalHitPlayer(game.secondPlayer, game.firstPlayer);
      }

      if (game.firstPlayer.currentHealth <= 0) {
        resolve(secondFighter);
      } else if (game.secondPlayer.currentHealth <= 0) {
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      const keyPressed = event.code;
      if (keyPressed === controls.PlayerOneBlock) {
        game.firstPlayer.blocked = false;
      } else if (keyPressed === controls.PlayerTwoBlock) {
        game.secondPlayer.blocked = false;
      }
      pressedKeysSuperHitFirst.clear();
      pressedKeysSuperHitSecond.clear();
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  return hitPower > blockPower ? hitPower - blockPower : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = Math.random() + 1;
  const { attack } = fighter;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  const { defense } = fighter;
  return defense * dodgeChance;
}

const pressedKeysSuperHitFirst = new Set();
const pressedKeysSuperHitSecond = new Set();

export function checkKeys(pressedKey) {
  const checkFirstPlayerKeys = [...controls.PlayerOneCriticalHitCombination].some((element) => element == pressedKey);
  const checkSecondPlayerKeys = [...controls.PlayerTwoCriticalHitCombination].some((element) => element == pressedKey);

  if (checkFirstPlayerKeys) {
    pressedKeysSuperHitFirst.add(pressedKey);
  } else if (checkSecondPlayerKeys) {
    pressedKeysSuperHitSecond.add(pressedKey);
  }

  if (pressedKeysSuperHitFirst.size === 3 && [...pressedKeysSuperHitFirst].every((element) => controls.PlayerOneCriticalHitCombination.includes(element))) {
    return 'first';
  } else if (pressedKeysSuperHitSecond.size === 3 && [...pressedKeysSuperHitSecond].every((element) => controls.PlayerTwoCriticalHitCombination.includes(element))) {
    return 'second';
  }
}


export function hitPlayer(attacker, defender) {
  if (!attacker.blocked && !defender.blocked) {
    const damage = getDamage(attacker, defender);
    defender.currentHealth -= damage;
    defender.bar.style.width = (defender.currentHealth * 100) / defender.health + '%';
  }
}

export function criticalHitPlayer(attacker, defender) {
  const currentTime = Date.now();
  if (currentTime - attacker.hitTime > 10000) {
    const damage = getHitPower(attacker);
    defender.currentHealth -= 2 * damage;
    defender.bar.style.width = (defender.currentHealth * 100) / defender.health + '%';
    attacker.hitTime = currentTime;
  }
}
